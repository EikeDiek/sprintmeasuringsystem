#pragma once
// LightBarrier.h
#ifndef LIGHTBARRIER_H
#define LIGHTBARRIER_H
#include <HardwareSerial.h>
#include "../connection/WifiConnection.h"
class LightBarrier
{
private:
    HardwareSerial* _HardSerial;
    short myId=-1;
    // HardwareSerial Serial;
public:
    void setSerial(HardwareSerial* serial);
    bool registerMe(WifiConnection* connection);
    bool registerMe();
    bool deregisterMe(); //withdraw
};

#endif
#include "LightBarrier.h"



void LightBarrier::setSerial(HardwareSerial* serial)
{
    _HardSerial = serial;
    //   this->_streamRef->println("TEST LightBarrier");
}

bool LightBarrier::registerMe(WifiConnection * connection)
{
    //check if wifi is connected
    if (!connection->isConnected())
        return false;



    connection->udpSend("register me");
    


    /* Notify central Unit
        UDP Broadcast
        type: "registration";
    */

    /* Receive registration acknowledgement from central Unit
         UDP receive
    */

    /* Save Central Unit IP and Port */
  //  Serial.println("TEST Register");
    this->_HardSerial->println("register me");
    return false;
}

bool LightBarrier::deregisterMe()
{
    /*
        UDP Broadcast
        type: "deregister";
    */
    return false;
}
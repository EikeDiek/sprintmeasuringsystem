#pragma once
#ifndef WIFICONNECTION_H
#define WIFICONNECTION_H

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <HardwareSerial.h>
#include "WiFiSettings.h"
//#include "../../rsc/Defines.h"

class WifiConnection
{
private:
	//const char* ssid = "DoEiHeJo";
	//const char* password = "*mRG6Ab305SJ#,~Z";

	WifiConnection(){}
	static bool instanceFlag;
	static WifiConnection* currentInstance;
	HardwareSerial* _HardSerial;
	WiFiUDP Udp;
	char incomingPacket[WiFiSettings::incomingPacketBufferSize];  // buffer for incoming packets

public:
	static WifiConnection* getInstance();
	void setSerial(HardwareSerial* serial);
	bool connect();
	bool disconnect();
	bool isConnected();
	bool udpReceive();
	unsigned char udpSend(char* msg);
	~WifiConnection()
	{
		instanceFlag = false;
	}

};

#endif


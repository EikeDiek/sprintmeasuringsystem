#include "WifiConnection.h"

bool WifiConnection::instanceFlag = false;
WifiConnection* WifiConnection::currentInstance = NULL;

WifiConnection* WifiConnection::getInstance()
{
	if (!instanceFlag)
	{
		currentInstance = new WifiConnection();
		WifiConnection::instanceFlag = true;
	}

	return currentInstance;
}

void WifiConnection::setSerial(HardwareSerial* serial)
{
	this->_HardSerial = serial;
	this->_HardSerial->println("setup WifiConnection serial");
}


bool WifiConnection::connect()
{
	unsigned long connectionTimeoutTime = millis() + WiFiSettings::maxConnectionTryTime;
	Serial.printf("Connecting to %s ", WiFiSettings::ssid);
	WiFi.begin(WiFiSettings::ssid, WiFiSettings::password);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");

		//Connnection Timeout
		if (millis() > connectionTimeoutTime)
		{
			Serial.printf("\nCould not Connect to \"%s\"\n", WiFiSettings::ssid);
			return false;
		}
			
	}
	Serial.println("WIFI connected");
	Udp.begin(WiFiSettings::localUdpPort);
	Serial.printf("Gateway IP: %s \n", WiFi.gatewayIP().toString().c_str());
	Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), WiFiSettings::localUdpPort);
	return true;
}

bool WifiConnection::disconnect()
{
	return WiFi.disconnect();
}



bool WifiConnection::isConnected() 
{	
	return WiFi.isConnected();
}



bool WifiConnection::udpReceive() {
	int packetSize = Udp.parsePacket();
	if (packetSize)
	{
		// receive incoming UDP packets
		Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
		int len = Udp.read(incomingPacket, 255);
		if (len > 0)
		{
			incomingPacket[len] = 0;
		}
		Serial.printf("UDP packet contents: %s\n", incomingPacket);

		// send back a reply, to the IP address and port we got the packet from
		return true;
	}
	return false;
}


unsigned char WifiConnection::udpSend(char* msg)
{
	
	IPAddress remoteIP = IPAddress(192,168,2,255);
	Udp.beginPacket(remoteIP, 53131);
	Serial.println(msg);
	Udp.write(msg);
	return Udp.endPacket();
}
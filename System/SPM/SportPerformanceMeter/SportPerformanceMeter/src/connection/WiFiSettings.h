#pragma once
#ifndef WIFISETTINGS_H
#define WIFISETTINGS_H
namespace WiFiSettings {
	static const char* ssid = "Battuta";
	static const char* password = "SportPerformance";//"*mRG6Ab305SJ#,~Z";

	const unsigned long maxConnectionTryTime = 20000;

	static const  unsigned int localUdpPort = 4210;  // local port to listen on
	static const int incomingPacketBufferSize = 255;
}

#endif
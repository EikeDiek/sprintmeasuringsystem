#pragma once
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#include "src/connection/WifiConnection.h"
#include "src/lightBarrier/LightBarrier.h"



enum state
{
	booted = 1 << 0,
	connected = 1 << 1,
	failedConnection = 1 << 2,
	registered = 1 << 3,
	activated = 1 << 4
};


int currentState = state::booted;


char  replyPacket[] = "Hi there! Got the message :-)";  // a reply string to send back
char* msg = "Echo message";

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 5;     // the number of the pushbutton pin
long debounce = 200;   // the debounce time, increase if the output flickers

// variables will change:
int buttonState = HIGH;         // variable for reading the pushbutton status
int previousState = LOW;
long timer = 0;         // the last time the output pin was toggled
LightBarrier* lightBarrier;
WifiConnection* connection;


long buttonTimer = 0;
long longPressTime = 2000;

boolean buttonActive = false;
boolean longPressActive = false;


//--------------------------------------------------------------
//SETUP
void setup()
{
	//setup serial printer
	Serial.begin(115200);


	//Connection with WiFi AP
	if (WifiReConnect())
		currentState = currentState | state::connected;
	else
		currentState = currentState | state::failedConnection;

	//setup Lightbarrier
	lightBarrier = new LightBarrier();
	lightBarrier->setSerial(&Serial);

	// initialize the pushbutton pin as an input:
	pinMode(buttonPin, INPUT);

}
//--------------------------------------------------------------


//--------------------------------------------------------------
//MAINLOOP
void loop()
{
	buttonEvent();

	 if (connection->udpReceive())
	 {
		 connection->udpSend(msg);
	 }
}
//--------------------------------------------------------------


void buttonEvent() {
	if (digitalRead(buttonPin) == HIGH)
	{
		if (buttonActive == false) 
		{
			buttonActive = true;
			buttonTimer = millis();
		}

		if ((millis() - buttonTimer > longPressTime) && (longPressActive == false)) 
		{
			longPressActive = true;
			Serial.println("button long pressed");
			WifiReConnect();
		}

	}
	else 
	{
		if (buttonActive == true) 
		{
			if (longPressActive == true) 
			{
				longPressActive = false;
			}
			else 
			{
				Serial.println("Button short touch");
				lightBarrier->registerMe(connection);
			}

			buttonActive = false;
		}
	}
}


bool WifiReConnect()
{	
	//setup Connection
	connection = WifiConnection::getInstance();
	if (connection->isConnected())
		connection->disconnect();
	connection->setSerial(&Serial);
	return connection->connect();
}
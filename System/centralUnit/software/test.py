import sys
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QPushButton, QLabel, QTreeWidget,QTreeWidgetItem
from PySide2.QtCore import QFile, QObject
class Form(QObject):
    
    def __init__(self, ui_file, parent=None):
        super(Form, self).__init__(parent)
        ui_file = QFile(ui_file)
        ui_file.open(QFile.ReadOnly)

        self.openedWindowList = []
        
        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()
        
        self.lbl = self.window.findChild(QLabel, 'label')
        
        self.tree = self.window.findChild(QTreeWidget, 'treeWidget')  # replace every 'tree' with your QTreeWidget

        self.d = loader.load("chooseParcours.ui")
        self.e = loader.load("sensorConnect.ui")
        
        
        btn = self.window.findChild(QPushButton, 'pushButton')
        btn.clicked.connect(self.add_items)

        btn_parcoursNext = self.d.findChild(QPushButton, "pB_next")
        btn_parcoursNext.clicked.connect(self.openSensorConnect)

        btn_sensorConnectDone = self.e.findChild(QPushButton, "pB_done")
        btn_sensorConnectDone.clicked.connect(self.closeDialogs)


        self.window.showMaximized()
        
        
        
    def ok_handler(self):
        self.lbl.setText("Hallo Welt")
        
    def add_items(self):
        rowcount = self.window.treeWidget.topLevelItemCount()
        self.window.treeWidget.addTopLevelItem(QTreeWidgetItem(rowcount))
        self.window.treeWidget.topLevelItem(rowcount).setText(0, 'Kraftmesser')        
        self.window.treeWidget.topLevelItem(rowcount).setText(1, '15')      
        self.window.treeWidget.topLevelItem(rowcount).setText(2, '192.168.2.199')
        self.openedWindowList.append(self.d)
        self.d.showMaximized()
        #self.d.exec()


    def openSensorConnect(self):
        self.openedWindowList.append(self.e)
        self.e.showMaximized()
        #self.e.exec()

    def closeDialogs(self):
        self.lbl.setText(str(len(self.openedWindowList)))
        for dialogs in self.openedWindowList:
            dialogs.close()


        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form('mainwindow.ui')
    sys.exit(app.exec_())
